package com.example.student.todo;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.app.Fragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


public class TodoActivity extends SingleFragmentActivity implements ActivityCallback{

    //This is how to build an ArrayList
    public ArrayList<Todoitem> Todoitems = new ArrayList<Todoitem>();
    public int currentTime;
    public int currentItem;
    public ArrayList<Todoitem> todoitems;

    @Override
    protected Fragment createFragment() {
        return new TodoListFragment();
    }

    @Override
    protected int getLayoutResId(){
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new itemViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void Todoitem(Uri TodoitemUri) {

    }
}
