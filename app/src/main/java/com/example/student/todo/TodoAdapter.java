package com.example.student.todo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.FragmentManager;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder> {

    private ArrayList<Todoitem> todoitems;
    private ActivityCallback activityCallback;

    public TodoAdapter(ActivityCallback activityCallback, ArrayList<Todoitem> todoitems){
        this.activityCallback = activityCallback;
        this.todoitems = todoitems;
    }


    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int i) {
holder.titleText.setText(todoitems.get(i).title);
    }

    @Override
    public int getItemCount() {
        return todoitems.size();
    }
}

