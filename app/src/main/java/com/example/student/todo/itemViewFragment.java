package com.example.student.todo;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class itemViewFragment extends Fragment {

    private ActivityCallback activityCallback;
    private TodoActivity activity;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (TodoActivity)activity;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_todo_list, container, false);

        TextView tv1 = (TextView) layoutView.findViewById(R.id.tv1);
        TextView tv2 = (TextView) layoutView.findViewById(R.id.tv2);
        TextView tv3 = (TextView) layoutView.findViewById(R.id.tv3);
        TextView tv4 = (TextView) layoutView.findViewById(R.id.tv4);

        tv1.setText("Name "+ activity.Todoitems.get(activity.currentItem).title);
        tv2.setText("Date Add "+ activity.Todoitems.get(activity.currentItem).title);
        tv3.setText("Date Due " + activity.Todoitems.get(activity.currentItem).title);
        tv4.setText("Category "+activity.Todoitems.get(activity.currentItem).title);


        WebView webView = (WebView)layoutView.findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());

        return layoutView;
    }



}
