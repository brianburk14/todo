package com.example.student.todo;


import android.content.Intent;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

public class itemViewActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private List<Todoitem> todoitems;
    private SensorManager sensorManager;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_web);
        viewPager = (ViewPager)findViewById(R.id.viewPager);

        Intent intent = getIntent();
        Uri TodoitemUri = intent.getData();

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Todoitem todoitem = todoitems.get(position);
                return null;
            }

            @Override
            public int getCount() {
                return todoitems.size();
            }
        });
for(int index = 0; index < todoitems.size(); index++){
    if(todoitems.get(index).category.equals(todoitems.toString())){
        viewPager.setCurrentItem(index);
        break;
    }
}
    };

}
