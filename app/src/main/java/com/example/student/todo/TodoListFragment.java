package com.example.student.todo;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.app.FragmentManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoListFragment extends Fragment {
    private RecyclerView recyclerView;
    private TodoActivity activity;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (TodoActivity) activity;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Todoitem i1 = new Todoitem("Post 1", "Post 1", "Post 1", "post 1");
        Todoitem i2 = new Todoitem("Post 2", "Post 2", "Post 2", "post 2");
        Todoitem i3 = new Todoitem("Post 3", "Post 3", "Post 3", "Post 3");
        Todoitem i4 = new Todoitem("Post 4", "Post 4", "Post 4", "Post 4");

        activity.Todoitems.add(i1);
        activity.Todoitems.add(i2);
        activity.Todoitems.add(i3);
        activity.Todoitems.add(i4);

        TodoAdapter adapter = new TodoAdapter(activityCallback, activity.Todoitems);
        recyclerView.setAdapter(adapter);

        return view;
    }
}